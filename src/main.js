import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import './assets/fonts/ionicons/css/ionicons.min.css'
import './assets/css/style.css'
import './assets/css/landing-1.css'
import './assets/css/landing-2.css'
import './assets/css/helpers.css'
import './assets/css/bootstrap/bootstrap.css'
import './assets/js/bootstrap.min.js'
import './assets/js/jquery.min.js'
import './assets/js/mains.js';


Vue.config.productionTip = false

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
